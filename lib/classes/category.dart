class Category {
  final int id;
  final String title;
  final String text;

  Category({this.id, this.title, this.text});

  factory Category.fromJson(Map<String, dynamic> json) {
    return Category(
      id: int.parse(json['id']),
      title: json['attributes']['title'],
      text: json['attributes']['text'],
    );
  }
}
