class Post {
  final int id;
  final String title;
  final String text;

  Post({this.id, this.title, this.text});

  factory Post.fromJson(Map<String, dynamic> json) {
    return Post(
      id: int.parse(json['id']),
      title: json['attributes']['title'],
      text: json['attributes']['text'],
    );
  }
}
