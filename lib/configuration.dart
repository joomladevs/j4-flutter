import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Configuration extends StatefulWidget {
  Configuration({Key key}) : super(key: key);

  @override
  _ConfigurationState createState() => _ConfigurationState();
}

class _ConfigurationState extends State<Configuration> {
  final _formKey = GlobalKey<FormState>();
  String _siteUrl = "";
  String _token = "";

  @override
  void initState() {
    _getSavedData();
  }

  Future<void> _getSavedData() async {

    SharedPreferences preferences = await SharedPreferences.getInstance();
    
    if (_siteUrl != '') {
      _siteUrl = preferences.getString('url');
    }

    if (_token != '') {
      _token = preferences.getString('token');
    }
  }

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text('Configuración'),
        backgroundColor: Color.fromRGBO(26, 70, 107, 1),
        leading: new IconButton(
          icon: new Icon(Icons.arrow_back),          
          onPressed: () => Navigator.of(context).pop(),
        ),
      ),
      body: Form(
          key: _formKey,
          child: Column(
            children: <Widget>[
              TextFormField(
                decoration: InputDecoration(labelText: 'Url de tu sitio'),
                initialValue: _siteUrl,
                onSaved: (val) => _siteUrl = val,
              ),
              TextFormField(
                decoration: InputDecoration(labelText: 'Token'),
                initialValue: _token,
                onSaved: (val) => _token = val,
              ),
              RaisedButton(onPressed: _saveConfiguration,
              child: Text('Guardar')),
            ],
          )),
    );
  }

  void _saveConfiguration() async {
    final FormState form = _formKey.currentState;

    if (form.validate()) {
      form.save();

      SharedPreferences preferences = await SharedPreferences.getInstance();
      
      preferences.setString('url', _siteUrl);

      preferences.setString('token', _token);

    }
  }
}
