
import 'package:shared_preferences/shared_preferences.dart';

import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:flutter/material.dart';

import 'package:j4/classes/category.dart';

Future<List> fetchCategories() async {
  List rawCategories;
  List categories = [];

 SharedPreferences preferences = await SharedPreferences.getInstance();

  String url = preferences.getString('url');
  String token = preferences.getString('token');

  final response = await http.get(
    url + '/api/index.php/v1/content/categories',
    headers: {
      'Authorization':
          'Bearer ' + token,
      "Accept": "*/*",
      "Accept-Encoding": "gzip, deflate",
      "Content-Type": "application/json"
    },
  );

  if (response.statusCode == 200) {
    // If server returns an OK response, parse the JSON.
    var myrawCategories = json.decode(response.body);
    rawCategories = myrawCategories['data'];
    for (var article in rawCategories) {
      categories.add(Category.fromJson(article));
    }
  } else {
    // If that response was not OK, throw an error.

  }

  return categories;
}

class Categories extends StatefulWidget {
  Categories({Key key}) : super(key: key);

  @override
  _CategoriesState createState() => _CategoriesState();
}

class _CategoriesState extends State<Categories> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
                appBar: AppBar(
          title: Text('Categorías'),
          backgroundColor: Color.fromRGBO(26, 70, 107, 1),
          leading: new IconButton(
               icon: new Icon(Icons.arrow_back),
               onPressed: () => Navigator.of(context).pop(),
              ), 
          ),
        body: Container(        
          child: FutureBuilder(
            future: fetchCategories(),
            builder: (BuildContext context, AsyncSnapshot snapshot) {
              if (snapshot.hasData) {
                return ListView.builder(
                    itemCount: snapshot.data.length,
                    itemBuilder: (context, index) {
                      Category article = snapshot.data[index];
                      return Container(
                        padding: EdgeInsets.fromLTRB(5, 10, 5, 10),
                        child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[                      
                          Container(
                            padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
                            child: Text(article.title,
                          style: TextStyle(fontSize: 30),
                          ),
                          ),
                        
                        ],
                      ),
                      );
                    });
              } else if (snapshot.hasError) {
                return Text('No puedo conseguir los datos');
              }

              return CircularProgressIndicator();
            },
          ),
        ),
      ),
    );
  }
}
