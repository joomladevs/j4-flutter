import 'package:html/parser.dart' show parse;

import 'dart:async';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:flutter/material.dart';


import 'package:j4/classes/post.dart';

Future<List> fetchArticles() async {
  SharedPreferences preferences = await SharedPreferences.getInstance();

  String url = preferences.getString('url');
  String token = preferences.getString('token');

  List rawArticles;
  List articles = [];

  final response = await http.get(
    url + '/api/index.php/v1/content/article',
    headers: {
      'Authorization':
          'Bearer ' + token,
      "Accept": "*/*",
      "Accept-Encoding": "gzip, deflate",
      "Content-Type": "application/json"
    },
  );

  if (response.statusCode == 200) {
    // If server returns an OK response, parse the JSON.
    var myrawArticles = json.decode(response.body);
    rawArticles = myrawArticles['data'];
    for (var article in rawArticles) {
      articles.add(Post.fromJson(article));
    }
  } else {
    // If that response was not OK, throw an error.

  }

  return articles;
}

class Articles extends StatefulWidget {
  Articles({Key key}) : super(key: key);

  @override
  _ArticlesState createState() => _ArticlesState();
}

class _ArticlesState extends State<Articles> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('Artículos'),
          backgroundColor: Color.fromRGBO(26, 70, 107, 1),
          leading: new IconButton(
            icon: new Icon(Icons.arrow_back),
            onPressed: () => Navigator.of(context).pop(),
          ),
        ),
        body: Container(
          child: FutureBuilder(
            future: fetchArticles(),
            builder: (BuildContext context, AsyncSnapshot snapshot) {
              if (snapshot.hasData) {
                return ListView.builder(
                    itemCount: snapshot.data.length,
                    itemBuilder: (context, index) {
                      Post article = snapshot.data[index];
                      return Container(
                        padding: EdgeInsets.fromLTRB(5, 10, 5, 10),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
                              child: Text(
                                article.title,
                                style: TextStyle(fontSize: 30),
                              ),
                            ),
                            Text(
                              parse(article.text).documentElement.text,
                              style: TextStyle(fontSize: 16, height: 1.5),
                            ),
                          ],
                        ),
                      );
                    });
              } else if (snapshot.hasError) {
                return Text('No puedo conseguir los datos');
              }

              return CircularProgressIndicator();
            },
          ),
        ),
      ),
    );
  }
}
